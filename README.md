# SSI4DTM Project Summary
For more information, please contact:
Giuseppe Roselli - email: projects@joinyourbit.com
## Introduction
### About Joinyourbit
Jonyourbit is an innovative and dynamic Digital Service Providers leading the digital journey of its Clients. We aim to support European and, eventually, global businesses’ digital adoption streamlining their document-based processes while reducing paper consumption to support the global ecosystem.

## SSI4DTM - Self-Sovereign Identity for Digital Transaction Management
Self-Sovereign Identity for Digital Transaction Management (SSI4DTM) project aims at implementing an innovative platform to execute any cross-border transactions such as NDAs, contracts, bids, etc. among trusted digital users. It enables users to:
1.	create and control their own Self-Sovereign Identity (SSI) identities;
1.	eSign and notarize the whole document-based transactions on blockchain;
1.	be recognized by other participant within the eSSIF-Lab European ecosystem with which the holder will be or is already in a business relationship.
The project outcome is to identify parties and allow them to access and scale the Digital Single Market (DSM), improve business performances and confidence, while connecting business peers with the eSSIF-Lab open community.
## Summary
### Business Problem
The adoption of digital technologies is changing the way companies compete in the global market, enabling new business models which supports the creation of the European Digital Single Market (DSM). This overwhelming transformation is also opening up new opportunities for individuals and businesses in the online environment under condition of fair competition and trust. While European DSM strategy is a great opportunity, there are still in place a number of problems, emerged in several business sector (e.g. car rental, financial, insurance, real estate, to name a fews):
1.	Trusted peers’ identification in the online environment;
2.	Lack of privacy, security and legal enforceability during the business transactions;
3.	Poor business performances (difficulty to scale at EU/global level, time consuming interactions, expensive paper-based processes, weak auditability).
### Technical Solution
SSI4DTM is a Software As a Service platform that relies on the use of Distribuited Ledger/Blockchain to support the registry of identifiers while implementing the integration between SSI and DTM services. The solution will provide:
- Attribute issuance (SSI credentials): Given Name and Surname, Passport ID, Passport issuance and expiration date, Home Address, Place of birth, date of birth, Title (e.g. legal representative), Organization (belong to). In addition, these attributes will be processed to issue the electronic advanced and qualified signature under the EU eIDAS trusted scheme and GDPR privacy policies. The eIDAS signature and DTM capabilities will streamline and secure the business transactions, while assuring legal enforceability;
- Attribute verification (SSI credentials): Know Your Customer (KYC) attributes issued by other eSSIF-LAB Issuer subgrantees. The result will enable fast onboarding and trusted peers’ recognition in the online environment;
- A suitable implementation of the SSI eIDAS Bridge, to digitally sign Verifiable Credentials using Joinyourbit qualified eIDAS eSeal. The eSeal will be issued through the eIDAS compliant Joinyourbit Registration Authority.

The technical solution belongs to the following components:

**SSI4DTM Web**. Web application component that allows user (Holder) to request and verify SSI credentials. During the SSI issuance session, the component request to fill the user data (web form), interact with SSI4STM APP (or SSI4DTM web agent) to generate/store credentials. During the SSI verification session, the component request user authentication via SSI credential, interact with SSI4DTM Backend Requestor to verify the credentials

**SSI4DTM Backend Requestor**. Software component that manages the session requests with the eSSIF-Lab Components (e.g. IRMA Server, mobile app, etc.). The component:


- issues SSI credentials using the eIDAS trusted schemes (via an eIDAS Qualified Certification Service Provider); referenced agencies such as InfoCamere (via Infocert authenticaded web services);

- verifies/discloses credential issued by other eSSIF-Lab Partecipants.

**SSI4DTM APP (SSI4DTM web agent)**. Mobile app (or hosted web agent) implementing SSI wallet.

**DTM Service Module**. Advanced Digital Transaction Management (DTM) software component based on the J2EE framework. The DTM Service Module designs and implements document-based workflows, defines user’s roles, digitally signs documents, automates workflow and contents (WCA) using Business Process Management (BPM) libraries, archives and notarizes on the blockchain, delivers documents to customers’ peers in a secure way, from any devices (e.g. mobiles). The DTM module is integrated with the SSI4DTM Web component.

## Integration with the eSSIF-Lab Functional Architecture and Community

SSI4DTM software components interact with the eSSIF-Lab ecosystem as follows:

**Session authorization layer**. SSI4DTM Backend Requestor sends the session request flow to the eSSIF-Lab Component. The SSI4DTM Backend Requestor receives back the assigned session token ID and the service endpoint (URL of the eSSIF-Lab Component back-end resource). The token ID allows SSI4DTM to participate to the eSSIF-Lab ecosystem, issuing/verifying SSI attributes.

**Issuance/verification layer**. SSI4DTM Backend Requestor sends the session request flow to the SSI4DTM APP. The request contains the issuing/verifying credentials and the session token ID. The layer enables SSI4DTM to issue/verify SSI attributes and request/provide business services from/to eSSIF-Lab Participants.

**Third-party layer**. SSI4DTM Backend Requestor sends data flow to eIDAS Trusted Service Provider (VCs to Aruba QTSP to registry users and request eIDAS qualified digital certificates), ComKYC (SSI credential issuance based on KYC). SSI4DTM Backend Requestor receives data flow from other eSSIF-Lab Participants such as SCEP (SSI credential and eIDAS qualified signature request to provide legal binding to smart contract and real world event data), GAYA (SSI credential and eIDAS Advanced Electronic Signature request to provide legal binding to specific documents digitally signed by notaries).

Signature and verification API will be provided to subgrantees aiming at eSealing/verifying VCs using Joinyourbit eSeal in accordance with the SSI eIDAS Bridge specifications.
